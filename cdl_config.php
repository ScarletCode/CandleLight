<?php
define('CDL_DB_SERVERNAME'	, 'localhost');
define('CDL_DB_USERNAME' 	, 'root');
define('CDL_DB_PASSWORD' 	, '');
define('CDL_DB_NAME' 		, 'cdl');
define('CDL_DB_PREFIX'		, 'cdl_');

define('CDL_BASE_URL'		, 'http://localhost/project/cdl/');
define('CDL_ADMIN_SLUG'		, 'cdl-admin');
define('CDL_ROOT'			, str_replace("\\", '/', __DIR__));