# CandleLight
CandleLight is a lightweight PHP based CMS.
It's not meant to be a replacement for other popular, feature rich CMSs.
Its built to be an easy to extend foundation for your own webapps, including a fully featured plugin and theme API, a clean and extensible backend and an easy to use frontendeditor.

## Current Status
Currently the Core is being developed. Meaning, there is not much to see yet. **Feel free to check back in again.**