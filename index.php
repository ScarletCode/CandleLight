<?php

/**
 *  .--.             . .     .             .    .  
 * :                 | |     |      o      |   _|_ 
 * |    .-.  .--. .-.| | .-. |      .  .-..|--. |  
 * :   (   ) |  |(   | |(.-' |      | (   ||  | |  
 *  `--'`-'`-'  `-`-'`-`-`--''---'-' `-`-`|'  `-`-'
 *                                     ._.'
 * 
 * a lightweight CMS made with love - just for you ( I swear )
 * @return awesomeness
 * @version 0.1
 */

// define cdl_include
include('cdl_config.php');
include('cdl/cdl_dependencies.php');
include('cdl/cdl_load.php');