<?php

/**
* This class handles general actions for the usercapabilitiese
*/
class cdl_capabilities{

	/**
	 * Trys to get a capabilitie from the db and returns the result
	 * @param string $key 	'id' or 'name'
	 * @param strin $value 	the keys value
	 * @return array 	
	 */
	public static function get($key, $value){
		global $cdl_db;
		$res = $cdl_db->get([$key => $value], 'capabilities', $key, $key);
		return $res;
	}

	/**
	 * Returns all currently saved capabilities
	 * @return array
	 */
	public static function get_all(){
		global $cdl_db;
		return $cdl_db->get_all('capabilities');
	}

	/**
	 * Adds an unassigned capabilitie to the database
	 * @param string $key 	'id' or 'name'
	 * @param strin $value 	the keys value
	 * @return boolean
	 */
	public static function add($value){
		$cap = cdl_capabilities::get('name', $value);
		if (empty($cap)) {
			$cap = array_pop($cap);
			global $cdl_db;
			$data = [
				'name' => $value
			];
			$cdl_db->create($data, 'capabilities');
			return true;
		}
		return false;
	}

	/**
	 * Removes a capabilitie and all its assignments from the database
	 * @param string $key 	'id' or 'name'
	 * @param strin $value 	the keys value
	 * @return boolean
	 */
	public static function remove($key, $value){
		$cap = cdl_capabilities::get('name', $value);
		if (!empty($cap)) {
			$cap = array_pop($cap);
			global $cdl_db;
			$data = [
				'id' => $cap['id']
			];
			$cdl_db->delete($data, 'usergroups_2_capabilities', 'capabilities');
			$cdl_db->delete($data, 'capabilities');
			return true;
		}
		return false;
	}
}