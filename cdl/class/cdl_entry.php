<?php


/**
* class cdl_entry
* This class handles a single post
*/
class cdl_entry{

	/**
	 * Empty entry data
	 * @var array
	 */
	public $entry = [
			'id' => null,
			'slug' => null,
			'title' => null,
			'content' => null,
			'type' => null,
			'parent' => null
		];

	/**
	 * sets the entry data
	 * @param array $entry the the desired content of the new entry object
	 * @access public
	 */
	public function __construct($entry = []){
		$this->entry = cdl_parseArgs($this->entry, $entry);
	}

	/**
	 * changes the entry content of the given field
	 * returns success
	 * @param string $field the field to be updated
	 * @param string $content the value of said field
	 * @return boolean
	 * @access public
	 */
	public function set_field($field, $content){
		if (array_key_exists($field, $this->entry) && $field != 'id'){
			$this->entry[$field] = $content;
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * determins the way this post is ment to safe and start the execution
	 * @return void
	 * @access public
	 */
	public function save(){
		if (isset($this->entry['id'])) {
			$this->update();
		}
		else{
			$this->create();
		}
	}

	/**
	 * updates the entry in the db with the current content of $entry
	 * @return void
	 * @access protected
	 */
	protected function update(){
		global $cdl_db;
		$cdl_db->update($this->entry, 'posts');
	}

	/**
	 * creates a new entry in the db with the current content of $entry
	 * @return void
	 * @access protected
	 */
	protected function create(){
		global $cdl_db;
		$cdl_db->create($this->entry, 'posts');
		$this->entry['id'] = $cdl_db->last_id();
	}

	/**
	 * deletes the entry from the db with the current 'id' in $entry
	 * @return void
	 * @access public
	 */
	public function delete(){
		global $cdl_db;
		$cdl_db->delete($this->entry, 'posts');
	}
}