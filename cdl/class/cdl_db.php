<?php


/**
* class cdl_db
* This class handles all database-interaction
*/
class cdl_db{

	/**
	 * the database class
	 * @var object
	 */
	protected $db;

	/**
	 * the table prefix
	 * @var string
	 */
	public $dbPrefix = CDL_DB_PREFIX;

	/**
	 * number of sql querys
	 * @var integer
	 */
	public $sql_count = 0;

	/**
	 * Establishes Database Connection
	 * @return void
	 * @access public
	 */
	public function __construct(){
		$db = new mysqli(CDL_DB_SERVERNAME, CDL_DB_USERNAME, CDL_DB_PASSWORD, CDL_DB_NAME);
		if ($db->connect_error) {
			die('Fehler beim Aufbau der Datenbankverbindung (' . $db->connect_errno . ') '
            . $db->connect_error);
		}
		$this->db = $db;
	}

	/**
	 * querys the Database with the given query
	 * @param string $query the sql query to be executed
	 * @param boolean if this function should return an array or return void
	 * @return mixed
	 * @access public
	 */
	public function sql($query, $get = true){
		$this->sql_count++;
		$result = $this->db->query($query);
		if ($get && $result) {
			return $result->fetch_all(MYSQLI_ASSOC);
		}		
	}

	/**
	 * Returns a sanitized version of the string
	 * @param string $str the string to be cleaned
	 * @return string
	 * @access public
	 */
	public function clean_string($str){
		$str = $this->db->real_escape_string($str);
		$str = stripslashes($str);
		return $str;
	}

	/**
	 * Closes the Database Connection
	 * @return void
	 * @access public
	 */
	public function close(){
		$this->db->close();
	}

	/**
	 * builds parts of the SQL query-string according to the given type
	 * @param array $data the data, which will be build into a sql query string
	 * @param string $type 'both' for key and value, 'key' for only key and 'data' for only data
	 * @return string
	 * @access protected
	 */
	protected function build_query_string($data, $type = 'both'){
		$str = '';
		$i = 0;
		foreach ($data as $key => $value) {

			$value = $this->db->real_escape_string($value);

			if ($key != 'id') {
				if ($i != 0)
					$str .= ',';

				if ($type === 'key')
					$str .= $key;
				elseif ($type === 'data')
					$str .= "'" . $value . "'";
				else
					$str .= $key . "='" . $value . "'";			
				
				$i++;
			}			
		}
		return $str;
	}

	/**
	 * updates an entry with the given data
	 * where the $field matches the $fieldname
	 * @param array $data the dataset to update
	 * @param string $table the table name, where the dataset is being updated
	 * @param string $field the column, where a value is checked
	 * @param string $fieldname the value to be checked in the WHERE statement
	 * @return void
	 * @access public
	 */
	public function update($data, $table, $field = 'id', $fieldname = 'id'){
		$sql = 'UPDATE ' . $this->dbPrefix . $table . ' SET ' . $this->build_query_string($data, 'both') . ' WHERE ' . $field . '="' . $data[$fieldname] . '"';
		$this->sql($this->clean_string($sql), false);
	}

	/**
	 * creates an entry with the given data
	 * @param array $data the dataset to insert
	 * @param string $table the table name, where the dataset is being inserted
	 * @return void
	 * @access public
	 */
	public function create($data, $table){
		$sql = 'INSERT INTO ' . $this->dbPrefix . $table . ' (' . $this->build_query_string($data, 'key') . ') VALUES (' . $this->build_query_string($data, 'data') . ')';
		$this->sql($this->clean_string($sql), false);
	}

	/**
	 * deletes the given entry
	 * @param array $data the dataset to delete
	 * @param string $table the table name, where the dataset is being deleted
	 * @param string $field the column, where a value is checked
	 * @param string $fieldname the value to be checked in the WHERE statement
	 * @return void
	 * @access public
	 */
	public function delete($data, $table, $field = 'id', $fieldname = 'id'){
		$sql = 'DELETE FROM ' . $this->dbPrefix . $table . ' WHERE ' . $field . '="' . $data[$fieldname] . '"';
		$this->sql($this->clean_string($sql), false);
	}

	/**
	 * delets a single entry with the given data
	 * where multiple fields do match the fieldnames
	 * @param array $data the dataset to look out for
	 * @param string $table the table name, where the dataset is being deleted
	 * @param array $field array of columns, where a value is checked
	 * @param array $fieldname array of values to be checked in the WHERE statement
	 * @return array
	 * @access public
	 */
	public function delete_and($data, $table, $fields = null, $fieldnames = null){
		$sql = 'DELETE FROM ' . $this->dbPrefix . $table . ' WHERE ';

		if ($fields != null && $fieldnames != null) {
			for ($i=0; $i <= (count($fields)-1); $i++) {
				if ($i != 0) {
					$sql .= ' AND ';
				}
				$sql .= $fields[$i] . "='" . $this->db->real_escape_string($data[$fieldnames[$i]]) . "'";
			}
		}
		else{
			$i = 0;
			foreach ($data as $key => $value) {
				if ($i != 0) {
					$sql .= ' AND ';
				}
				$sql .= $key.'="' . $value . '"';
				$i++;
			}
		}
		return $this->sql($this->clean_string($sql), false);
	}

	/**
	 * gets all entrys from a single database
	 * where the result of another selection matches
	 * @param array $data 	the dataset to get data from
	 * @param string $table 	the table name, where the dataset is being fetched
	 * @param string $field the column, where a value is checked
	 * @param string $fieldname the value to be checked in the WHERE statement
	 * @param string $check the column, where a inner selection is is checked
	 * @return array
	 * @access public
	 */
	public function get_from($data, $table, $field, $fieldname, $check){
		$sql = 'SELECT * FROM ' . $this->dbPrefix . $table. ' WHERE ' . $check . '=';
		$sql .= '(SELECT ' . $check . ' FROM ' . $this->dbPrefix . $table . ' WHERE ';
		$sql .= $field . '=' . $data[$fieldname] . ')';
		return $this->sql($this->clean_string($sql), true);
	}

	/**
	 * gets a single entry with the given data
	 * where the $field matches the $fieldname
	 * @param array $data the dataset to look out for
	 * @param string $table the table name, where the dataset is being fetched
	 * @param string $field the column, where a value is checked
	 * @param string $fieldname the value to be checked in the WHERE statement
	 * @return array
	 * @access public
	 */
	public function get($data, $table, $field = 'id', $fieldname = 'id'){
		$sql = 'SELECT * FROM ' . $this->dbPrefix . $table . ' WHERE ' . $field . "='" . $this->db->real_escape_string($data[$fieldname]) . "'";
		return $this->sql($this->clean_string($sql), true);
	}

	/**
	 * gets a single entry with the given data
	 * where multiple fields do match the fieldnames
	 * @param array $data the dataset to look out for
	 * @param string $table the table name, where the dataset is being fetched
	 * @param array $field array of columns, where a value is checked
	 * @param array $fieldname array of values to be checked in the WHERE statement
	 * @return array
	 * @access public
	 */
	public function get_and($data, $table, $fields = null, $fieldnames = null){
		$sql = 'SELECT * FROM ' . $this->dbPrefix . $table . ' WHERE ';

		if ($fields != null && $fieldnames != null) {
			for ($i=0; $i <= (count($fields)-1); $i++) {
				if ($i != 0) {
					$sql .= ' AND ';
				}
				$sql .= $fields[$i] . "='" . $this->db->real_escape_string($data[$fieldnames[$i]]) . "'";
			}
		}
		else{
			$i = 0;
			foreach ($data as $key => $value) {
				if ($i != 0) {
					$sql .= ' AND ';
				}
				$sql .= $key.'="' . $value . '"';
				$i++;
			}
		}
		return $this->sql($this->clean_string($sql), true);
	}

	/**
	 * gets multiple entrys using the LIKE statement
	 * @param  string  $table the table name, where the dataset is being fetched
	 * @param  string  $fieldname the column to check
	 * @param  string  $value the value to look out for
	 * @param  boolean $contains if the value should only be contained in the field
	 * @return  array
	 * @access public
	 */
	public function get_like($table, $field = 'id', $value = '', $contains = false){
		$contains = ($contains) ? '%' : '';
		$sql = 'SELECT * FROM ' . $this->dbPrefix . $table . ' WHERE ' . $field . " LIKE '" . $contains . $this->db->real_escape_string($value) . $contains . "'";
		return $this->sql($this->clean_string($sql), true);
	}

	/**
	 * Get multiple entrys over 3 relational tables
	 * @param array $tables 	list of tables to passed
	 * @param array $values 	list of values to be checked in a table
	 * @param array $where 		list of where statements on the final query
	 * @param boolean $deep_search if where statements are for each layer and not just the first one
	 * @return array
	 * @access public
	 */
	public function get_join($tables, $values, $where, $deep_search = false){
		$i = 0;
		$sql = 'SELECT t_' . (count($tables) - 1) . '.* FROM `' . $tables[0] . '` AS t_' . $i . ' ';
		for ($i=1; $i < count($tables); $i++) { 
			$sql .= 'LEFT JOIN `' . $tables[$i] . '` AS t_' . $i . ' ';
			$sql .= 'ON t_' . ($i - 1) .'.' . $values[$i][0] . ' = ' . 't_' . $i .'.' . $values[$i][1] . ' ';
		}
		$sql .= 'WHERE ';
		$i = 0;
		foreach ($where as $key => $value) {
			if ($i != 0) {
				$sql .= 'AND ';
			}
			$num = ($deep_search) ? $i : 0 ;
			$sql .= 't_' . $num . '.' . $key . '="' . $value . '" ';
			$i++;
		}
		return $this->sql($this->clean_string($sql), true);
	}

	/**
	 * get all entrys from the given table
	 * @param string $table the table name, where the dataset is being fetched
	 * @return array
	 * @access public
	 */
	public function get_all($table){
		$sql = 'SELECT * FROM ' . $this->dbPrefix . $table;
		return $this->sql($this->clean_string($sql), true);
	}

	/**
	 * Returns the last inserted id
	 * @return int
	 * @access public
	 */
	public function last_id(){
		return $this->db->insert_id;
	}
}