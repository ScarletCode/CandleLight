<?php


/**
* 
*/
class cdl_languagelink{
	
	/**
	 * Default Language Link
	 * @var array
	 */
	public $link = null;

	/**
	 * List of posts, in the current language link
	 * @var array
	 */
	public $posts = [];

	/**
	 * FIlls the current $Link and $posts data with the given results
	 * returns success
	 * @param array $res 	database result
	 * @return boolean
	 */
	protected function fill($res){
		if (!empty($res)) {
			foreach ($res as $post) {
				array_push($this->posts, $post);
			}
			$this->link = array_pop($res)['translation'];
			return true;
		}
		return false;
	}

	/**
	 * Gets the current language link by a member posts entry
	 * @param int $id 	the entry id contained in the language link
	 * @return boolean
	 */
	public function get_link_by_member($id){
		$data = [
			'id' => $id
		];
		global $cdl_db;
		$res = $cdl_db->get_from($data, 'posts_2_languages', 'post', 'id', 'translation');
		return $this->fill($res);
	}

	/**
	 * Gets the current language link by its id
	 * @param int $id 	the language links ID
	 * @return void
	 */
	public function get_link_by_id($id){
		$data = [
			'id' => $id
		];
		global $cdl_db;
		$res = $cdl_db->get($data, 'posts_2_languages', 'translation', 'id');
		return $this->fill($res);
	}

	/**
	 * Checks if the given language from this languagelink is filled
	 * @param string $lang 	the language slug
	 * @return void
	 */
	public function has_lang($check){
		foreach ($this->posts as $post) {
			$lang = array_pop(cdl_languages::get($post['language'], 'id'));
			if ($lang['name'] == $check) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Adds a post to the given languag of the current languagelink
	 * @param string $lang 	the language slug
	 * @param array $post 	the post to be set for the given lanuage
	 * @return void
	 */
	public function add_post($lang, $post){
		$lang = cdl_languages::get($lang, 'name');
		if (!empty($lang) && $this->link != null) {
			$lang = array_pop($lang);

			$data = [
				'post' => $post,
				'language' => $lang['id'],
				'translation' => $this->link
			];
			
			global $cdl_db;
			$cdl_db->create($data, 'posts_2_languages');
			array_push($this->posts, $data);
			return true;
		}
		return false;
	}

	/**
	 * Removes a given post from the current languagelink
	 * @param string $lang 	the language slug
	 * @param array $id 	the post to be deleted from the current language link
	 * @return void
	 */
	public function remove_post($id){
		if ($this->link != null) {
			foreach ($this->posts as $key => $post) {
				if ($post['post'] == $id) {
					
					global $cdl_db;
					$data = [
						'translation' => $this->link,
						'post' => $id
					];
					$fields = [
						'translation',
						'post'
					];
					$cdl_db->delete_and($data, 'posts_2_languages', $fields, $fields);
					unset($this->posts[$key]);
					return true;
				}
			}
		}
		return false;
	}
}