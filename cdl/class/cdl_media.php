<?php


/**
* class cdl_media
* This class handles multiple media files
*/
class cdl_media{
	
	/**
	 * array of uploaded files
	 * @var array
	 */
	public $uploads = [];

	/**
	 * array of single file objects
	 * @var array
	 */
	public $files = [];

	function __construct(){
	}

	/**
	 * checks if files have been uploaded
	 * @return boolean
	 * @access public
	 */
	public function has_uploads(){
		if (count($_FILES) != 0) {
			return true;
		}
		return false;
	}

	/**
	 * fetches uploaded files from the $_FILES and builds the files array
	 * @return array $args
	 * @access public
	 */
	public function get_uploads(){
		$upload = [];
		for ($i = 0; $i < count($_FILES['files']['name']); $i++) { 
			$tmp = [];
			foreach ($_FILES['files'] as $key => $value) {
				$tmp[$key] = $value[$i];
			}
			array_push($upload, $tmp);
		}
		$this->uploads = $upload;
		return $upload;
	}

	/**
	 * gets all media files from the database
	 * @return array
	 * @access public
	 */
	public function get_all(){
		global $cdl_db;
		$files = $cdl_db->get_all('media');
		$this->append_files($files);
		return $files;
	}

	/**
	 * get all files containend in the given path
	 * @param  string $dir the directory, where the media files are being fetched from
	 * @return array
	 * @access public
	 */
	public function get_folder($dir = ''){
		global $cdl_db;
		$dir = realpath(CDL_ROOT . '/cdl_content/uploads/'. $dir);
		if ($dir) {
			$files = $cdl_db->get_like('media', 'path', $dir, true);
			$this->append_files($files);
			return $files;
		}
		return [];
	}

	/**
	 * builds the file objects from the database array and appends them to the current $files
	 * @param  array $files the file data to build the file objects from
	 * @return array
	 * @access protected
	 */
	protected function append_files($files){
		foreach ($files as $file) {
			array_push($this->files, new cdl_file($file));
		}
		return $this->files;
	}
}
/*

$media = new cdl_media();
$media->get_folder('xyz');
foreach ($media->files as $file) {
	$file->move('abxasdas');
}
print_r($media->files);

$medias = new cdl_media();
if ($medias->has_uploads()) {
	$files = $medias->get_uploads();
	foreach ($files as $file) {
		$path = $file['tmp_name'];
		$file = new cdl_file($file);
		$file->move('xyz', $path);
		$file->rename('img');
	}
}


?>

<form style="margin:200px 0 0 200px" method="post" action="#test" enctype="multipart/form-data">
	<input type="file" name="files[]" multiple>
	<input name="test" type="submit" value="Upload">
</form>

 */