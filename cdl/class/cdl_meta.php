<?php

/**
* General Post Meta handler
*/
class cdl_meta{

	/**
	 * determines the way this meta is ment to be saved
	 * and kicks off its execution
	 * @param string $id 	the post id
	 * @param string $name 	the meta name
	 * @param string $content 	the metas
	 * @return void
	 */
	public static function save($id, $name, $content){
		$check = self::get($id, $name);
		if (empty($check)) {
			self::add($id, $name, $content);
		}
		else{
			self::update($check[0]['id'], $name, $content);
		}
	}

	/**
	 * updates a new entry meta to the database
	 * @param $where the meta id
	 * @param $name the meta name
	 * @param $content the metas content
	 * @return void
	 */
	protected static function update($where, $name, $content){
		global $cdl_db;
		$data = array(
				'id' => $where,
				'slug' => $name,
				'content' => $content
			);
		$cdl_db->update($data, 'metas');
	}

	/**
	 * adds a new entry meta to the database
	 * @param $id 	the post id
	 * @param $name the meta name
	 * @param $content the metas content
	 * @return void
	 */
	protected static function add($id, $name, $content){
		global $cdl_db;
		$meta = array(
				'slug' => $name,
				'content' => $content
			);
		$cdl_db->create($meta, 'metas');
		$relation = array(
				'post' => $id,
				'meta' => $cdl_db->last_id()
			);
		$cdl_db->create($relation, 'posts_2_metas');
	}

	/**
	 * deletes a meta entry from the database
	 * @param $id the post id
	 * @param $name the meta name to be deleted
	 * @return void
	 */
	public static function delete($id, $name){
		global $cdl_db;
		$delete = self::get($id, $name);
		if (!empty($delete)) {
			$data = array(
					'id' => $delete[0]['id'],
					'slug' => $name
				);
			$cdl_db->delete($data, 'metas');
			$data = array(
					'post' => $id,
					'name' => $delete[0]['id']
				);
			$cdl_db->delete($data, 'posts_2_metas', 'post', 'post');
		}	
	}

	/**
	 * gets a entry meta from the database
	 * @param $id the post id
	 * @param $name the meta name to be fetched
	 * @return void
	 */
	public static function get($id, $name){
		global $cdl_db;
		$tables = [
			'cdl_posts_2_metas',
			'cdl_metas'
		];
		$values = [
			null,
			['meta', 'id'],
			['post', 'id']
		];
		$where = [
			'post' => $id,
			'slug' => $name
		];

		$return = [];
		$res = $cdl_db->get_join($tables, $values, $where, true);
		foreach ($res as $cap) {
			array_push($return, $cap);
		}
		return $return;
	}
}