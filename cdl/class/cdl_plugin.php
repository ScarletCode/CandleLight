<?php


/**
* class cdl_theme
* This class handles a single plugin
*/
class cdl_plugin{

	/**
	 * Gets a list of all currently existing themes
	 * @return array containing all plugin objects
	 */
	public static function get_plugins(){
		$plugins = [];
		$dir =  CDL_ROOT . '/cdl_content/plugins';
		foreach (scandir($dir) as $plugin) {
			if (is_dir($dir . '/' . $plugin) && $plugin != '.' && $plugin != '..') {
				$plugins[$plugin] = new self($dir . '/' . $plugin, $plugin);
			}
		}
		return $plugins;
	}

	/**
	 * Inits all currently activated plugins
	 * @return void
	 */
	public static function init_plugins(){
		$plugins = self::get_plugins();
		foreach ($plugins as $plugin) {
			$plugin->init();
		}
	}

	/**
	 * the plugin directory
	 * @var string
	 */
	public $dir;

	/**
	 * the plugin url
	 * @var string
	 */
	public $path;

	/**
	 * the plugin name
	 * @var string
	 */
	public $name;

	/**
	 * the plugin status
	 * @var string
	 */
	public $active = false;

	/**
	 * constructs the plugin in the given directory
	 * @param string $dir this plugins directory
	 * @param string $name this plugins name
	 * @access public
	 */
	public function __construct($dir, $name){
		$this->dir = $dir;
		$this->name = $name;
		$this->path = 'not yet';
		$this->active = $this->is_active();
	}

	/**
	 * checks if the current Plugin is active
	 * @return boolean
	 * @access protected
	 */
	protected function is_active(){

		// default data
		$entry = [
			'name' => 'activePlugins',
			'value' => serialize([])
		];

		$result = cdl_options::get($entry['name']);

		// Create entry if it doesnt exist yet
		if (empty($result)) {
			cdl_options::save($entry['name'], $entry['value']);
			return false;
		}
		else{
			// check plugin status
			if (in_array($this->name, unserialize(array_pop($result)['value']))) {
				return true;
			}
			else{
				return false;
			}
		}
	}

	/**
	 * activates the current plugin
	 * @return void
	 * @access public
	 */
	public function activate(){
		echo "test";
		if (!$this->active ) {

			// default data
			$entry = [
				'name' => 'activePlugins'
			];

			// add current plugin to the activePlugins-List
			$result = cdl_options::get($entry['name']);
			$result = unserialize(array_pop($result)['value']);
			array_push($result, $this->name);

			// save updated List
			$entry['value'] = serialize($result);
			cdl_options::save($entry['name'], $entry['value']);

			// update plugin status
			$this->active = true;
		}	
	}

	/**
	 * deactivates the current plugin
	 * @return void
	 * @access public
	 */
	public function deactivate(){
		if ($this->active) {

			// default data
			$entry = [
				'name' => 'activePlugins'
			];

			// remove current plugin from the activePlugins-List
			$result = cdl_options::get($entry['name']);
			$result = unserialize(array_pop($result)['value']);
			foreach ($result as $key => $value) {
				if ($value === $this->name) {
					unset($result[$key]);
				}
			}

			// save updated List
			$entry['value'] = serialize($result);
			cdl_options::save($entry['name'], $entry['value']);

			// update pluing status
			$this->active = false;
		}
	}

	/**
	 * initializes the plugin
	 * @return void
	 * @access public
	 */
	public function init(){
		if ($this->active) {
			$includes = [
				'index.php'
			];
			foreach ($includes as $include) {
				safeInclude($this->dir . '/' . $include);
			}
		}	
	}
}
