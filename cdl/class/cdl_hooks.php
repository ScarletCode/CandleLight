<?php


/**
* class cdl_hooks
* This class handles the Hooks API for the entire CMS
*/
class cdl_hooks{

	/**
	 * Multidimensional array, containing all hooks
	 * @var array
	 */
	public $hooks = [];

	/**
	 * Multidimensional array, containing all callbacks
	 * @var array
	 */
	public $callbacks = [];
	
	/**
	 * adds a new Hook and executes all callbacks hooked to this hook
	 * @param  string $name 		the hooks name
	 * @param  array  $parameters 	the hooks parameters passed to the callbacks
	 * @param  string $location 	the file dir, where this hook is being applied right now
	 * @return mixed
	 * @access public
	 */
	public function new_hook($name, $parameters = [], $location = NULL){
		$hook = [
			'name' => $name,
			'parameters' => $parameters,
			'location' => $location
		];
		$this->hooks[$name] = $hook;

		if (isset($this->callbacks[$name])) {
			foreach ($this->callbacks[$name] as $callback) {
				$output = call_user_func_array($callback, $parameters);
			}
			return $output;
		}
		return array_pop($parameters);
	}

	/**
	 * adds a Callback to the List of callbacks
	 * @param  string $hook 		the hook, this callback is being hooked to
	 * @param  string $function 	the callback function
	 * @return boolean
	 * @access public
	 */
	public function new_callback($hook, $function = ''){
		if (function_exists($function)) {
			if (!isset($this->callbacks[$hook])) {
				$this->callbacks[$hook] = [];
			}
			array_push($this->callbacks[$hook], $function);
			return false;
		}
		return false;		
	}

	/**
	 * returns an array containing all hooks with all infos
	 * @return array
	 * @access public
	 */
	public function get_hooks(){
		return $this->hooks;
	}
}
/*
function myFunction($content){
	return str_replace(' ', '_', $content);
}
$hook->new_callback('test', 'myFunction');
$test = 'Ich habe heute mal wieder ganz schön viel vor.';
$test = $hook->new_hook('test', [$test], __FILE__);
echo $test;
*/