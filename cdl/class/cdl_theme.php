<?php


/**
* class cdl_theme
* This class handles a single theme
*/
class cdl_theme{

	/**
	 * Gets a list of all currently existing themes
	 * @return array containing all plugin objects
	 */
	public static function get_themes(){
		$themes = [];
		$dir =  CDL_ROOT . '/cdl_content/themes';
		foreach (scandir($dir) as $theme) {
			if (is_dir($dir . '/' . $theme) && $theme != '.' && $theme != '..') {
				$themes[$theme] = new self($dir . '/' . $theme, $theme);
			}
		}
		return $themes;
	}

	/**
	 * Inits all currently activated themes
	 * @return void
	 */
	public static function init_themes(){
		$themes = self::get_themes();
		foreach ($themes as $theme) {
			$theme->init();
		}
	}

	/**
	 * the themes directory
	 * @var string
	 */
	public $dir;

	/**
	 * the themes url
	 * @todo  to be filled
	 * @var string
	 */
	public $path;

	/**
	 * the themes name
	 * @var string
	 */
	public $name;

	/**
	 * constructs the theme in the given directory
	 * @param string $dir this themes directory
	 * @param string $name this themes name
	 * @access public
	 */
	public function __construct($dir, $name){
		$this->dir = $dir;
		$this->name = $name;
		$this->path = 'not yet';
	}

	/**
	 * checks if this theme is active
	 * @return boolean
	 * @access public
	 */
	public function check(){
		$array = [
			'name' => 'activeTheme',
			'value' =>$this->name
		];
		$result = cdl_options::get($array['name']);

		// check theme status from db
		if (empty($result) || array_pop($result)['value'] != $this->name) {
			return false;
		}
		return true;
	}

	/**
	 * activates the current theme
	 * @return void
	 * @access public
	 */
	public function activate(){
		$array = [
			'name' => 'activeTheme',
			'value' => $this->name
		];
		cdl_options::save($array['name'], $array['value']);
	}

	/**
	 * initializes the theme
	 * @return void
	 * @access public
	 */
	public function init(){
		if ($this->check()) {
			$includes = [
					'functions.php',
					'index.php'
				];
			foreach ($includes as $include) {
				safeInclude($this->dir . '/' . $include);
			}
			return true;
		}
		return false;
	}
}