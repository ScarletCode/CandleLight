<?php


/**
* This class handles the CMS global option table
*/
class cdl_options{

	/**
	 * handles multiple options at once
	 * @param string $type 	the way the given options should be handled
	 * @param array $options 	list of options to be handled
	 * @return void
	 */
	public static function bulk($type, $options){
		if ($type == 'delete') {
			foreach ($options as $option) {
				self::delete($option['name']);
			}
		}
		else{
			foreach ($options as $option) {
				self::save($option['name'], $option['value']);
			}
		}
	}

	/**
	 * Deletes the given option from the database
	 * @param string $name 	the option name to be deleted
	 * @return void
	 */
	public static function delete($name){
		global $cdl_db;
		$data = [
			'name' => $name
		];
		$cdl_db->delete($data, 'options', 'name', 'name');
	}
	
	/**
	 * determines the way this option is ment to be saved
	 * and kicks off its execution
	 * @param string $name 	the options name
	 * @param string $value the new options value
	 * @return void
	 */
	public static function save($name, $value){
		$get = self::get($name);
		if (empty($get)) {
			self::add($name, $value);
		}
		else{
			self::update($name, $value);
		}
	}

	/**
	 * adds a new option to the database
	 * @param string $name 	the options name
	 * @param stsring $value the new options value
	 * @return void
	 */
	protected static function add($name, $value){
		global $cdl_db;
		$data = [
			'name' => $name,
			'value' => $value
		];
		$cdl_db->create($data, 'options');
	}

	/**
	 * updates an option in the database
	 * @param string $name the options name
	 * @param string $value the new options value
	 * @return void
	 */
	protected static function update($name, $value){
		global $cdl_db;
		$data = [
			'name' => $name,
			'value' => $value
		];
		$cdl_db->update($data, 'options', 'name', 'name');
	}

	/**
	 * get a single option from the database
	 * @param string $name the options name to get the value from
	 * @return mixed
	 */
	public static function get($name){
		global $cdl_db;
		$data = ['name' => $name];
		return $cdl_db->get($data, 'options', 'name', 'name');
	}

	/**
	 * Gets all current options from the database
	 * @return array
	 */
	public static function get_all(){
		global $cdl_db;
		return $cdl_db->get_all('options');
	}

	/**
	 * fetches multiple options from the database
	 * @param array $names 	list of names to get the values from
	 * @return array
	 */
	public static function fetch($names){
		$return = [];
		foreach ($names as $name) {
			$res = self::get($name);
			if (!empty($res)) {
				array_push($return, $res);
			}
		}
		return $return;
	}
}