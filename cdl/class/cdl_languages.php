<?php

/**
 * This class handles the content languages
 */
class cdl_languages{

	/**
	 * Gets all currently registred languages from the database
	 * @return array
	 */
	public static function get_all(){
		global $cdl_db;
		return $cdl_db->get_all('languages');
	}

	/**
	 * Gets a single language by slug from the database
	 * @param string $slug 	the lanugages name
	 * @return array
	 */
	public static function get($value, $by = 'name'){
		global $cdl_db;
		$data = [
			$by => $value
		];
		return $cdl_db->get($data, 'languages', $by, $by);
	}

	/**
	 * Deletes the given language from the database
	 * @param string $slug 	the languages name to be deleted
	 * @todo delte lang-links to this language
	 * @return void
	 */
	public static function delete($slug){
		global $cdl_db;
		$data = [
			'name' => $slug
		];
		$cdl_db->delete($data, 'languages', 'name', 'name');
	}

	/**
	 * Adds a new language to the database
	 * @param string $slug 	the new languages name
	 * @return void
	 */
	public static function add($slug){
		$get = self::get($slug);
		if (empty($get)) {
			global $cdl_db;
			$data = [
				'name' => $slug
			];
			$cdl_db->create($data, 'languages');
		}
	}

	/**
	 * Renames the name of the given language in the database
	 * @param string $slug 	the old name
	 * @param string $slug 	the new name
	 * @return void
	 */
	public static function rename($slug, $new){
		global $cdl_db;
	}
}
