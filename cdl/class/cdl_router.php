<?php


/**
* This class handles the basic routing in the cms
*/
class cdl_router{


	/**
	 * If routs to the admin panel
	 * @var boolean
	 */
	protected $admin 	= false;

	/**
	 * If routs on the root directory
	 * @var boolean
	 */
	protected $home 	= false;

	/**
	 * If route points at an ID
	 * @var int
	 */
	protected $direct;

	/**
	 * If no page with this route has been found
	 * @var boolean
	 */
	protected $error	= false;

	/**
	 * The current route (ASC)
	 * @var array
	 */
	protected $route 	= [];

	/**
	 * The current route result
	 * @var array
	 */
	protected $result;
	
	/**
	 * Sets the basic request route
	 */
	public function __construct(){
		$scheme = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';

		$dir_replace = str_replace($scheme . '://' . $_SERVER['SERVER_NAME'], '', CDL_BASE_URL);
		$request = str_replace($dir_replace, '', $_SERVER['REQUEST_URI']);

		$request = trim(preg_replace('/\?.*/', '', $request), '/');
		$request = explode('/', $request);

		if (empty($request[0])) {

			// Direct Page call via ID
			if (isset($_GET['p']) && !empty($_GET['p']) && is_numeric($_GET['p'])) {
				$this->direct = intval($_GET['p']);
			}
			else{

				// Home Page
				$this->home = true;
			}
		}
		elseif ($request[0] == CDL_ADMIN_SLUG){

			// Admin Page
			$this->admin = true;
			unset($request[0]);
			if (empty($request)) {

				// Admin Home
				$this->home = true;
			}
		}

		// Pages
		$this->route = $request;


		// If on Frontpage and pages are set
		// check if we are on a 404 page
		if (!$this->home && !$this->admin && empty($this->direct)) {
			$this->error = !$this->check_route();
		}
	}

	/**
	 * Checks if the curent route exists
	 * @return [boolean] true if the current route is valid
	 * @access protected
	 */
	protected function check_route(){
		global $cdl_db;

		$parent = 0;
		for ($key=0; $key <= (count($this->route) - 1); $key++) { 

			$result = $cdl_db->get_and(['slug' => $this->route[$key], 'parent' => $parent], 'posts', ['slug', 'parent'], ['slug', 'parent']);
			if (empty($result)) {
				return false;
			}
			$parent = $result[0]['id'];
		}
		$this->result = array_pop($result);
		return true;
	}

	/**
	 * Redirects to the given URL
	 * @param string $link the target url
	 * @param int $type status code
	 * @return void
	 */
	public function redirect($link, $type = 302){
		header('Location: ' . $link, true, $type);
		exit;
	}

	/**
	 * Gets the current route
	 * @return [array] [array containing all possiblilitys and its value]
	 * @access public
	 */
	public function get_route(){

		static $route;
		if (isset($route)){
			return $route;
		}

		$route = new stdClass();
		$route->admin = $this->admin;
		$route->home = $this->home;
		$route->direct = $this->direct;
		$route->route = $this->route;
		$route->error = $this->error;
		$route->result = $this->result;

		return $route;
	}
}