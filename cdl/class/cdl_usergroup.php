<?php


/**
* This class handles a single usergroup
*/
class cdl_usergroup{

	public $usergroup = [
		'id' => null,
		'name' => null,
		'title' => null,
		'description' => null
	];

	protected $capabilities = [];
	
	function __construct($usergroup = []){
		$this->usergroup = cdl_parseArgs($this->usergroup, $usergroup);

		if ($this->usergroup['id'] != null) {
			if ($this->usergroup['name'] == null) {
				$check = $this->get();
				if (!$check) {
					$this->usergroup['id'] = null;
				}
			}
			$this->get_capabilities($this->usergroup['id']);
		}
	}

	/**
	 * Gets the current usergroup by the set id fromt the db
	 * fill up $usergroup on success
	 * @return boolean 	success
	 */
	public function get($field = 'id', $fieldname = 'id'){
		global $cdl_db;
		$res = $cdl_db->get($this->usergroup, 'usergroups', $field, $field);
		if (empty($res)) {
			return false;
		}
		$this->usergroup = array_pop($res);
		return true;
	}

	/**
	 * Gets the capabilities currently assigned to this usergroup
	 * @return array the currently assigned capabilites
	 */
	public function get_capabilities($id){
		global $cdl_db;
		$tables = [
			'cdl_usergroups',
			'cdl_usergroups_2_capability',
			'cdl_capabilities'
		];
		$values = [
			null,
			['id', 'usergroup'],
			['capability', 'id']
		];
		$where = [
			'id' => $id
		];
		
		$return = [];
		$res = $cdl_db->get_join($tables, $values, $where);
		foreach ($res as $cap) {
			array_push($return, $cap);
		}
		$this->capabilities = $return;
		return $return;
	}

	/**
	 * Checks if the usergroup has a specific capabilitie
	 * @param string @cap 	name of the capabilitie
	 * @return boolean
	 */
	public function can($cap){
		foreach ($this->capabilities as $capabilitie) {
			if ($capabilitie['name'] == $cap) {
				return true;
			}
		}
		return false;
	}

	/**
	 * changes the usergroup content of the given field
	 * returns success
	 * @param string $field the field to be updated
	 * @param string $content the value of said field
	 * @return boolean
	 * @access public
	 */
	public function set_field($field, $content){
		if (array_key_exists($field, $this->usergroup) && $field != 'id'){
			$this->usergroup[$field] = $content;
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * Adds a capabilitie to the current usergroup
	 * @param string $key 		'id' or 'name'
	 * @param string $value 	the keys value
	 * @return boolean 	returns success
	 */
	public function add_capabilitie($key, $value){
		global $cdl_db;
		$res = cdl_capabilities::get($key, $value);
		if (!empty($res)) {
			$res = array_pop($res);
			if (!$this->can($res['name'])) {
				$data = [
					'usergroup' => $this->usergroup['id'],
					'capability' => $res['id']
				];
				$cdl_db->create($data, 'usergroups_2_capability');
				$this->get_capabilities($this->usergroup['id']);
				return true;
			}
		}
		return false;
	}

	/**
	 * Removes a capabilitie from the current usergroup
	 * @param string $key 	'id' or 'name'
	 * @param string $value 	the keys value
	 * @return boolean 		returns success
	 */
	public function remove_capabilitie($key, $value){
		global $cdl_db;
		$res = cdl_capabilities::get($key, $value);
		if (!empty($res)) {
			$res = array_pop($res);
			if ($this->can($res['name'])) {
				$data = [
					'usergroup' => $this->usergroup['id'],
					'capability' => $res['id']
				];
				$cdl_db->delete_and($data, 'usergroups_2_capability');
				$this->get_capabilities($this->usergroup['id']);
				return true;
			}
		}
		return false;
	}

	/**
	 * determins the way this usergroup is ment to safe and start the execution
	 * @return void
	 * @access public
	 */
	public function save(){
		if (isset($this->usergroup['id'])) {
			$this->update();
		}
		else{
			$this->create();
		}
	}

	/**
	 * updates the usergroup in the db with the current content of $usergroup
	 * @return void
	 * @access protected
	 */
	protected function update(){
		global $cdl_db;
		$cdl_db->update($this->usergroup, 'usergroups');
	}

	/**
	 * creates a new usergroup in the db with the current content of $usergroup
	 * @return void
	 * @access protected
	 */
	protected function create(){
		global $cdl_db;
		$cdl_db->create($this->usergroup, 'usergroups');
		$this->usergroup['id'] = $cdl_db->last_id();
	}

	/**
	 * deletes the usergroup from the db with the current 'id' in $usergroup
	 * @return void
	 * @access public
	 */
	public function delete(){
		global $cdl_db;
		$cdl_db->delete($this->usergroup, 'usergroups');
	}
}