<?php


/**
* class cdl_content
* This class handles multiple entrys
*/
class cdl_content{

	/**
	 * the posts
	 * @var array
	 */
	public $content = [];

	/**
	 * sets the entry data
	 * @param array $args the desired entrys to fetch from the db
	 * @access public
	 */
	public function __construct($args = []){
		$defaults = [
				'id' => null,
				'slug' => null,
				'title' => null,
				'content' => null,
				'type' => null,
				'parent' => null
			];
		$args = cdl_parseArgs($defaults, $args);

		$entrys = $this->fetch_entrys($args);
		foreach ($entrys as $entry) {
			array_push($this->content, new cdl_entry($entry));
		}
	}

	/**
	 * fetches matching entrys from database 
	 * @param array @args args of the to be fetched entrys
	 * @return array
	 * @access protected
	 */
	protected function fetch_entrys($args){

		$check = [];
		foreach ($args as $key => $value) {
			if ($value != null) {
				$check[$key] = $value;
			}
		}

		global $cdl_db;
		return $cdl_db->get_and($check, 'posts');
	}

	/**
	 * apply action defined by $type to all entrys in $content
	 * @param string $type the action type
	 * @return void
	 * @access public
	 */
	public function bulk_handle($type = 'none'){
		if ($type == 'delete') {
			foreach ($this->content as $entry) {
				$entry->delete();
			}
		}		
	}
}