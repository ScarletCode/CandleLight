<?php


/**
* class cdl_admin
* This class handles the basic admin page displays
*/
class cdl_admin{
	
	/**
	 * All current pages and subpages of the admin panel
	 * @var array
	 */
	protected $pages = [];

	/**
	 * Default Page array
	 * @var array
	 */
	protected $page = [
		'slug' => null,
		'title' => null,
		'callback' => null,
		'order' => null,
		'childs' => []
	];

	/**
	 * Route to the admin panel dashboard
	 * @var array
	 */
	protected $dashboard = [];

	/**
	 * Trys to add a new page to the admin handler
	 * @param string $slug     the new pages slug
	 * @param string $title    the new pages title
	 * @param string $callback the new pages content callback
	 * @param string $parent   the new pages parent slug
	 * @return void
	 */
	public function add_page($slug, $title, $callback, $order = null, $parent = null){
		$page = $this->page;
		$page['slug'] = $slug;
		$page['title'] = $title;
		$page['callback'] = $callback;
		$page['order'] = $order;

		if ($parent == null) {
			$this->pages[$page['slug']] = $page;
		}
		else{
			$this->pages = $this->insert_child($this->pages, $page, $parent);
		}
	}

	/**
	 * Sets the route to the admin panel dashboard
	 * which can be accessed directly or if we are in the
	 * home route
	 * @param array $route 		route to the admin dashboard
	 * @return void
	 */
	public function set_dashboard($route){
		$this->dashboard = $route;
	}

	/**
	 * Trys to add a child into a parent page
	 * @param  array $pages  list of the currently handled pages
	 * @param  array $page   current page data
	 * @param  string $parent parent page slug
	 * @return array         updated list of handled pages
	 */
	protected function insert_child($pages, $page, $parent){
		foreach ($pages as $num => $check) {
			if ($check['slug'] == $parent) {
				$pages[$num]['childs'][$page['slug']] = $page;
			}
			else{
				$pages[$num]['childs'] = $this->insert_child($check['childs'], $page, $parent);
			}
		}
		return $pages;
	}

	/**
	 * Kicks of the sorting for the current page and all its child pages
	 * @param  array $page the page
	 * @return array
	 */
	protected function sort_childs($page){
		uasort($page['childs'], [$this, 'sort_pages_by_order']);
		foreach ($page['childs'] as $key => $value) {
			$page['childs'][$key] = $this->sort_childs($value);
		}
		return $page;
	}

	/**
	 * Sorts the current pages by its order key
	 * @param  array $a page 1 to compare
	 * @param  array $b page 2 to compare
	 * @return int
	 */
	public function sort_pages_by_order($a, $b){
		if ($a['order'] == $b['order']) {
			return 0;
		}
		return ($a['order'] < $b['order']) ? -1 : 1;
	}

	/**
	 * Returns the current ordered List of pages with all childpages
	 * @return array
	 */
	public function get_pages(){
		static $pages;
		if (isset($pages)){
			return $pages;
		}
		$pages = $this->pages;
		uasort($pages, [$this, 'sort_pages_by_order']);
		foreach ($pages as $key => $page) {
			$pages[$key] = $this->sort_childs($page);
		}
		return $pages;
	}

	/**
	 * Returns the current page by the given route
	 * @param  array $route the current admin route
	 * @return array        the current page
	 */
	public function get_page($route){
		static $final;
		if (isset($final)) {
			return $final;
		}

		if (empty($route) && isset($this->dashboard)) {
			$route = $this->dashboard;
		}

		$lvl = $this->get_pages();
		$i = 0;
		foreach ($route as $page) {
			$catch = false;
			foreach ($lvl as $key => $value) {
				if ($key == $page) {
					$final = $value;
					$lvl = $value['childs'];
					$catch = true; 
				}
			}
			if (!$catch) {
				$final = [];
				break;
			}
			$i++;
		}
		return $final;
	}

	/**
	 * Kicks of the callback for the given Page
	 * @param array $page the current page
	 * @return void
	 */
	public function page_init($page){
		if (function_exists($page['callback'])) {
			call_user_func($page['callback'], $page);
		}
	}
}

/*
function post_list($page){
	echo '<pre>';
	print_r($page);
	echo '</pre>';
}



$cdl_admin = new cdl_admin();


$cdl_admin->add_page('dashboard', 'Dashboard', 'post_list', 1);
$cdl_admin->set_dashboard(['dashboard']);

$cdl_admin->add_page('post', 'Beitrag', 'post_list', 1);
$cdl_admin->add_page('post-list', 'Alle Seiten', 'post_list', 2, 'post');
$cdl_admin->add_page('post-create', 'Seite erstellen', 'post_create', 1, 'post');

$cdl_admin->add_page('page', 'Seite', 'page', 2);
$cdl_admin->add_page('page-create', 'Seite erstellen', 'page_-create', 1, 'page');
$cdl_admin->add_page('page-list', 'Alle Seiten', 'page_list', 2, 'page');


$route = $cdl_router->get_route();

$page = $cdl_admin->get_page($route->route);
$cdl_admin->page_init($page);
*/