<?php


/**
* class cdl_file
* This class handles a single media file
*/
class cdl_file{

	/**
	 * the file
	 * @var array
	 */
	public $file = [
		'id'	=> null,
		'name' 	=> null,
		'type' 	=> null,
		'url' 	=> null,
		'path' 	=> null
	];
	
	/**
	 * sets the file data
	 * @param array $file the the desired data for the new file object
	 * @access public
	 */
	public function __construct($file){
		$this->file = cdl_parseArgs($this->file, $file);
	}

	/**
	 * Updates this files database entry
	 * @return void
	 * @access protected
	 */
	protected function update_location(){
		global $cdl_db;
		if ($this->file['id'] == null) {
			$cdl_db->create($this->file, 'media');
			$this->file['id'] = $cdl_db->lastId();
		}
		else{
			$cdl_db->update($this->file, 'media');
		}
		
	}

	/**
	 * moves the target to the destination and updates its location in the database
	 * @param  string $destination 	path of the destination
	 * @param  string $target 		path to the target file (optional)
	 * @return boolean              success of the action
	 * @access public
	 */
	public function move($destination, $target = null){

		// If target is not set manually (i.e. on file upload) use $file path
		if ($target == null) {
			$target = $this->file['path'] . '/' . $this->file['name'];
		}
		$destination = CDL_ROOT . '/cdl_content/uploads/'. $destination;
		$path = $destination . '/';

		// Create dir if it does not exist
		if (!is_dir($destination)) {
			mkdir($destination, 0777, true);
		}

		// Count up name if allready exist
		$targetFile = realpath($path) . '/' . $this->file['name'];
		if (is_file($targetFile)) {
			$this->rename($this->file['name']);
		}

		// Trys to move the file
		$rename = rename($target, $targetFile);
		if ($rename) {

			// Updates db and object
			$this->file['path'] = realpath($path);
			$this->update_location();
			return true;
		}
		return false;
	}

	/**
	 * deletes the file and removes it from the database
	 * @return boolean		success of the action
	 * @access public
	 */
	public function delete(){
		global $cdl_db;

		// Trys to delete file
		$unlink = unlink($this->file['path'] . '/' . $this->file['name']);
		if ($unlink) {

			// Delete from db and update object
			$this->file = [];
			$cdl_db->delete($this->file, 'media');
			return true;
		}
		return false;
	}

	/**
	 * renames the file and updates the database
	 * @param  string $name  new file of the file
	 * @return boolean       success of the action
	 * @access public
	 */
	public function rename($name){
		$explode = explode('.', $this->file['name']);
		$extension = array_pop($explode);

		// count up name if already exist
		$baseName = $name;
		$i = 1;
		while (is_file($this->file['path'] . '/' . $name . '.' . $extension)) {
			$name = $baseName . $i;
			$i++;
		}
		$name = $name . '.' . $extension;

		// trys to rename
		$rename = rename($this->file['path'] . '/' . $this->file['name'], $this->file['path'] . '/' . $name);	
		if ($rename) {

			// Updates db and object
			$this->file['name'] = $name;
			$this->update_location();
			return true;
		}
		return false;
	}
}
