<?php


/**
* class cdl_user
* This class handles a single user
*/
class cdl_user{

	/**
	 * Empty user data
	 * @var array
	 */
	public $user = [
			'id' => null,
			'name' => null,
			'mail' => null,
			'password' => null
		];

	/**
	 * Usergroups of the current user
	 * @var array
	 */
	protected $usergroups = [];

	/**
	 * Ruleset for the user data
	 * @var array
	 */
	protected $rules = [
			'name' => [
					'min-length' 	=> 5,
					'max-length' 	=> 20
				],
			'mail' => [
					'check-fun' 	=> 'cdl_isMail'
				]
		];

	/**
	 * If user data is correct
	 * @var boolean
	 */
	protected $checked = false;

	/**
	 * If th euser was logged in right now
	 * @var boolean
	 */
	protected $login = false;

	/**
	 * sets the user data
	 * @param array $user the the desired userdata of the new user object
	 * @access public
	 */
	public function __construct($user = []){
		$this->user = cdl_parseArgs($this->user, $user);
	}

	/**
	 * changes the user-data of the given field
	 * returns success
	 * @param string $field 	the field to be updated
	 * @param string $content 	the fields value
	 * @return bool
	 * @access public
	 */
	public function set_field($field, $content){
		if (array_key_exists($field, $this->user) && $field != 'id'){
			$this->checked = false;
			$this->user[$field] = $content;
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * checks the user against the cdl_user ruleset and returns the result
	 * @return boolean
	 * @access public
	 */
	public function check(){
		$this->checked = checkRules($this->user, $this->rules);
		return $this->checked;
	}

	/**
	 * updates the user in the db with the current content of $user
	 * @return void
	 * @access public
	 */
	public function update(){
		if ($this->checked) {
			global $cdl_db;
			$cdl_db->update($this->user, 'users');
		}
	}

	/**
	 * creates a new user in the db with the current content of $user
	 * @return void
	 * @access public
	 */
	public function create(){
		var_dump($this->checked);
		if ($this->checked) {
			global $cdl_db;
			$cdl_db->create($this->user, 'users');
			$this->user['id'] = $cdl_db->last_id();
		}
	}

	/**
	 * deletes the user from the db with the current 'id' in $user
	 * @return void
	 * @access public
	 */
	public function delete(){
		if ($this->checked) {
			global $cdl_db;
			$cdl_db->delete($this->user, 'users');
		}
	}

	/**
	 * get user by the current $user['name']
	 * @return array
	 * @access public
	 */
	public function get($by = 'name'){
		global $cdl_db;
		$return = $cdl_db->get($this->user, 'users', $by, $by);
		$this->user = array_pop($return);

		$this->get_usergroups($this->user['id'], 'id');

		return $this->user;
	}

	/**
	 * login the current $user
	 * @return boolean
	 * @todo $_SESSION
	 * @access public
	 */
	public function login(){
		if ($this->check_login()) {
			setcookie('cdl_login', $this->user['name'] . '_' . cdl_passwordEncrypt($this->user['password']), time() + 60*60*24 );
			$this->login = true;
			return true;
		}
		return false;
	}

	/**
	 * logout the current session
	 * @return void
	 * @access public
	 */
	public function logout(){
		setcookie('cdl_login', false, 1);
	}

	/**
	 * checks if the login is valid
	 * @return boolean
	 * @access protected
	 */
	protected function check_login(){
		$user = $this->get();
		if ($this->user['password'] === $user['password']) {
			return true;
		}
		return false;
	}

	/**
	 * checks if the current $user is logged in
	 * @return boolean
	 */
	public function check_logged_in(){
		if ($this->login) {
			return true;
		}
		if (isset($_COOKIE['cdl_login'])) {
			$userData = split('_', $_COOKIE['cdl_login']);
			$this->set_field('name', $userData[0]);
			if (cdl_passwordEncrypt($this->get()['password']) === $userData[1]) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the usergroups of the current user
	 * @param int  $íd 	the current us
	 * @return array  list of usergroups which the current user is assigned to
	 */
	protected function get_usergroups($id, $key = 'id'){
		global $cdl_db;

		// REMOVE CDL UPDATE JOIN IN DB CLASS
		$tables = [
			'cdl_users',
			'cdl_users_2_usergroups',
			'cdl_usergroups'
		];
		$values = [
			null,
			['id', 'user'],
			['usergroup', 'id']
		];
		$where = [
			$key => $id
		];

		$return = [];
		$groups = $cdl_db->get_join($tables, $values, $where);

		foreach ($groups as $group) {
			if ($group['id'] === null) {
				continue;
			}
			array_push($return, new cdl_usergroup($group));
		}

		$this->usergroups = $return;
		return $return;
	}

	/**
	 * Adds a usergroup to the current user
	 * @param int $id 	the usergroup id
	 * @return boolean
	 */
	public function add_usergroup($key, $value){
		global $cdl_db;
		$res = $cdl_db->get([$key => $value], 'usergroups', $key, $key);
		if (!empty($res)) {
			$res = array_pop($res);
			if (!$this->in_usergroup('id', $res['id'])) {
				$data = [
					'user' => $this->user['id'],
					'usergroup' => $res['id']
				];
				$cdl_db->create($data, 'users_2_usergroups');
				$this->get_usergroups($this->user['id']);
				return true;
			}
		}
		return false;
	}

	/**
	 * Removes a usergroup from the current user
	 * @param int $id 	the usergroup id
	 * @return void
	 */
	public function remove_usergroup($key, $value){
		global $cdl_db;
		$res = $cdl_db->get([$key => $value], 'usergroups', $key, $key);
		if (!empty($res)) {
			$res = array_pop($res);
			if ($this->in_usergroup('id', $res['id'])) {
				$data = [
					'user' => $this->user['id'],
					'usergroup' => $res['id']
				];
				$cdl_db->delete_and($data, 'users_2_usergroups');
				$this->get_usergroups($this->user['id']);
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the current user has the given capabilitie
	 * @param string $cap 	capabilitie name to be checked
	 * @return boolean
	 */
	public function can($cap){
		foreach ($this->usergroups as $group) {
			if ($group->can($cap)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the current user is in the given usergroup
	 * @param string $key 		the value to be checked i.e. 'id' or 'name'
	 * @param string $group 	the keys value
	 * @return boolean
	 */
	public function in_usergroup($key, $value){
		foreach ($this->usergroups as $group) {
			if ($group->usergroup[$key] == $value) {
				return true;
			}
		}
		return false;
	}
}


/*


$user = new cdl_user();
if (isset($_POST['login'])){
	$user->set_field('name', $_POST['name']);
	$user->set_field('password', cdl_passwordEncrypt($_POST['password']));
	$user->login();
}
$login = $user->check_logged_in();

if ($login) {
	echo "logged in";
	var_dump($user->can('test'));
}
else{
	echo "not logged in";
	?>

	<form action="#" method="post">
		<input type="text" name="name" value="" placeholder="name">
		<input type="password" name="password" value="" placeholder="pw">
		<input type="submit" name="login" value="login">
	</form>

	<?php
}


*/