<?php


/**
 * function cdl_parseArgs
 * fills the default array with the values of a second given array
 * 
 * @param array $defaults array of default values
 * @param array $args array of args to be inserted into the default array
 * @return array
 */
function cdl_parseArgs($defaults, $args){
	foreach ($args as $key => $value) {
		if (array_key_exists($key, $defaults)) {
			$defaults[$key] = $value;
		}
	}
	return $defaults;
}

/**
 * function cdl_passwordEncrypt
 * encrypts the given String (mainly passwords)
 * 
 * @param string $pw the string to be hashed
 * @return return string
 */
function cdl_passwordEncrypt($pw){
	return hash('md5', $pw);
}

/**
 * function cdl_isMail
 * checks if the given String is a valid E-Mail Adress
 * 
 * @param string $str the string to check for
 * @return boolean
 */
function cdl_isMail($str){
	if (filter_var($str, FILTER_VALIDATE_EMAIL)) {
		return true;
	}
	return false;
}


/**
 * function checkRules
 * returns if no data-input-rules have been broken
 * 
 * @param array $obj the data to check agains the rules
 * @param array $rules ruleset array wich applies to the given data
 * @return boolean
 */
function checkRules($data, $rules){

	// innocent until proven guilty
	$valid = true;

	// parse data
	foreach ($data as $key => $value) {
		if (array_key_exists($key, $rules)) {

			// get ruleset for each dataset
			foreach ($rules[$key] as $type => $check) {

				// if rules apply to current dataset
				if ($check != null) {

					// check data
					switch ($type) {
						case 'min-value':
							if ($value < $check){
								$valid = false;
							}
							break;
						case 'max-value':
							if ($value > $check){
								$valid = false;
							}
							break;
						case 'min-length':
							if (strlen($value) < $check){
								$valid = false;
							}
							break;
						case 'max-length':
							if (strlen($value) > $check){
								$valid = false;
							}
							break;
						case 'check-fun':
							if (function_exists($check) && !call_user_func($check, $value)) {
								$valid = false;
							}
							break;
					}
				}
			}

		}	
	}
	return $valid;
}


/**
 * function safeInclude
 * includes the file from the relative dir if possible
 * 
 * @param string $dir the dir to be included
 * @return boolean
 */
function safeInclude($dir){
	if (file_exists($dir) && is_readable($dir)) {
		include $dir;
		return true;
	}
	return false;
}





?>