<?php

/*
 * ========================================
 * classes
 * ========================================
 */
include('class/cdl_router.php');		// Router
include('class/cdl_db.php');			// Database Handler
include('class/cdl_hooks.php');			// Hook API
include('class/cdl_options.php');		// CDL Option API
include('class/cdl_content.php');		// Multiple Post Handler
include('class/cdl_entry.php');			// Single Post Handler
include('class/cdl_meta.php');			// Post Meta Handler
include('class/cdl_languages.php'); 	// Multilanguage Handler
include('class/cdl_languagelink.php'); 	// Content Translation Handler
include('class/cdl_user.php');			// User Handler
include('class/cdl_capabilities.php');	// User rights Handler
include('class/cdl_usergroup.php');		// Usergroup Handler
include('class/cdl_media.php');			// Multiple Media Handler
include('class/cdl_file.php');			// Single Media Handler
include('class/cdl_plugin.php'); 		// Plugin Handler
include('class/cdl_theme.php'); 		// Theme Handler
include('class/cdl_admin.php'); 		// Admin Panel Handler

// multilang support
// shortcode api // include views
// content types
// link content types

/*
 * ========================================
 * global functions
 * ========================================
 */
include('function/general.php'); 	// general use
// include('functions/content.php'); 	// content

?>